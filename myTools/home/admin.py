from django.contrib import admin
from .models import AppDetail

@admin.register(AppDetail)
class AppDetailAdmin(admin.ModelAdmin):
    list_display = ('app_name', 'author', 'likes', 'comment', 'number_of_clicks')
    search_fields = ('app_name', 'author')

    fieldsets = (
        (None, {
            'fields': ('app_name', 'author')
        }),
        ('Details', {
            'fields': ('likes', 'comment', 'number_of_clicks')
        }),
        ('Content', {
            'fields': ('chinese_content', 'english_content')
        }),
        ('Link', {
            'fields': ('url',)
        }),
    )
