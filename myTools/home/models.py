from django.db import models

class AppDetail(models.Model):
    app_name = models.TextField()
    author = models.TextField()
    likes = models.IntegerField()
    comment = models.IntegerField()
    number_of_clicks = models.IntegerField()
    chinese_content = models.TextField()
    english_content = models.TextField()
    url = models.TextField()
