from django.urls import path, include
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.home, name='home'),
    path('add_title', views.add_title_tool, name='add_title_tool'),
    path('line_notify/', include('line_notify.urls', namespace='line_notify')),

]
