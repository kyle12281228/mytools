from django.shortcuts import render
from .models import AppDetail
import random

def home(request):
	apps = AppDetail.objects.all()
	numbers = [random.randint(1,3) for _ in range(len(apps))]
	apps = zip(apps, numbers)
	return render(request, 'home/home.html', {'apps':apps})

def add_title_tool(request):
	# py-script written tool
	return render(request, 'home/tag.html')