from django.urls import path
from . import views


app_name = 'myURL'

urlpatterns = [
    path('', views.shorten_url, name='shorten_url'),
    path('<str:short_code>/', views.redirect_original_url, name='redirect_original_url'),
]

