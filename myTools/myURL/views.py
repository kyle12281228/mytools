from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .forms import ShortenURLForm
from .models import ShortURL
from myQrcode.views import generate_qr_return_path

def shorten_url(request):
    if request.method == 'POST':
        form = ShortenURLForm(request.POST)
        if form.is_valid():
            original_url = form.cleaned_data['original_url']
            short_url = ShortURL.objects.create(original_url=original_url)
            short_url.save()
            # http://achen.xn--n7q482c.tw/url/
            shortened_url = request.build_absolute_uri('/url/') + short_url.short_code
            shortened_url = shortened_url.replace('xn--n7q482c', '冠曄')
            qr_img_path = generate_qr_return_path(shortened_url)
            context = {'shortened_url': shortened_url, 'img':qr_img_path}
            return render(request, 'myURL/shorten_url.html', context)
    else:
        form = ShortenURLForm()
    return render(request, 'myURL/index.html', {'form': form})


def redirect_original_url(request, short_code):
    short_url = get_object_or_404(ShortURL, short_code=short_code)
    original_url = short_url.original_url

    return render(request, 'myURL/redirect.html', {'original_url': original_url})

