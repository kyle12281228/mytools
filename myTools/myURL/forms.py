from django import forms

class ShortenURLForm(forms.Form):
    original_url = forms.URLField(label='Original URL', max_length=200, required=True)
