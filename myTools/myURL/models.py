import uuid
from django.db import models

class ShortURL(models.Model):
    short_code = models.CharField(max_length=4, unique=True)
    original_url = models.URLField()
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.id:
            # Generate a new short code only when creating a new instance
            while True:
                short_code = uuid.uuid4().hex[:4]
                if not ShortURL.objects.filter(short_code=short_code).exists():
                    self.short_code = short_code
                    break
        super().save(*args, **kwargs)

    def __str__(self):
        return self.short_code

