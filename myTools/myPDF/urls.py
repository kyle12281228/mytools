from django.urls import path
from . import views


app_name = 'myPDF'

urlpatterns = [
    path('', views.remove_password, name='remove_password'),
]

