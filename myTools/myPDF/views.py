import io
import pikepdf
from django.http import FileResponse, HttpResponse
from django.shortcuts import render

def remove_password(request):
    if request.method == 'POST':
        password = request.POST.get('password')
        pdf_file = request.FILES.get('pdf_file')
        
        output_pdf = io.BytesIO()  # Create an in-memory file-like object
        
        try:
            with pikepdf.open(pdf_file, password=password) as pdf:
                pdf.save(output_pdf)  # Save the modified PDF data to the in-memory object
        except pikepdf.PasswordError:
            return HttpResponse("Incorrect password provided.")
        
        output_pdf.seek(0)  # Reset the position to the beginning of the file
        
        response = FileResponse(output_pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="modified.pdf"'
        
        return response
    
    return render(request, 'myPDF/remove_password.html')
