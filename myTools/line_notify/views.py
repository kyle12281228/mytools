from django.shortcuts import render, redirect
import requests
from .forms import ContactForm

def LineMessage(token, msg):
	headers = {
		"Authorization": "Bearer "+token,
		"Content-Type":"application/x-www-form-urlencoded"
	}
	payload = {
      'message' : msg,
					}

	r = requests.post('https://notify-api.line.me/api/notify', headers = headers, params = payload)
	return r.status_code



def send_message(request):
	token = 'VFTNuLsKI9hItMfPY4tXwtGgoaCZCqNCCoDmYx6OBo3'
	if request.method == 'POST':
		name = request.POST.get('name')
		email = request.POST.get('email')
		content = request.POST.get('content')
		user_ip = request.META.get('HTTP_X_FORWARDED_FOR') or \
                  request.META.get('HTTP_X_REAL_IP') or \
                  request.META.get('REMOTE_ADDR')
		message = f"\n來自 {name} 的訊息：\n{content}\nemail: {email}\nIP: {user_ip}"
		LineMessage(token, message)
		return redirect('/#contact')

	return render(request, 'line_notify/contact_line.html', {})

