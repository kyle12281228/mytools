from django.apps import AppConfig


class LineNotifyConfig(AppConfig):
    name = 'line_notify'
