from django.shortcuts import render

from qrcode import QRCode
from random import randint
import base64
from io import BytesIO

def generate_qr_return_path(content, color=(0,0,0)):
	qr = QRCode(
	    version = 1,
	    box_size = 10,
	    border = 2,
	)

	qr.make(fit=True)
	qr.add_data(content)
	img = qr.make_image(fill_color=color, back_color="white")
	# path = "./media/qr.png"
	# img.save(path)
	# return path[1:]
	buffered = BytesIO()
	img.save(buffered, format="PNG")
	qr_image_base64 = base64.b64encode(buffered.getvalue()).decode("utf-8")
	result = f"data:image/png;base64,{ qr_image_base64 }"
	return result
	

def create_qr(request):
	if request.method == 'POST':
		content = request.POST.get('content')
		color = request.POST.get("color-picker")

		img_path = generate_qr_return_path(content, color)
		random_para = randint(0,100)

		return render(request, 'myQrcode/create_qr.html', {'img':img_path, 'random_para':random_para})

	return render(request, 'myQrcode/create_qr.html', {})