from django.urls import path
from . import views


app_name = 'myQrcode'

urlpatterns = [
    path('', views.create_qr, name='create_qr'),
]

